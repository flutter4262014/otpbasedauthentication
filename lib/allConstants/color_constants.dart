import 'package:flutter/material.dart';

class ColorConstants {
  static const primaryColor = Colors.purple;
  static const secondaryColor = Colors.white;
  static const primaryBackgroungColor = Color(0xfff7f6fb);
}