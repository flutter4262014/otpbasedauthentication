import 'package:flutter/material.dart';
import 'package:otp_based_authentication/allScreens/registration_screen.dart';

import '../allConstants/color_constants.dart';
import '../allConstants/language_constants.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ColorConstants.primaryBackgroungColor,
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 32),
        child: Column(
          children: [
            SizedBox(
              height: 18,
            ),
            Image.asset(
              'assets/images/Vision_1.png',
              width: 240,
            ),
            SizedBox(
              height: 85,
            ),
            Text(
              translation(context).home_welcome_message1,
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 18,
            ),
            Text(
              translation(context).home_welcome_message2,
              style: TextStyle(fontSize: 14, color: Colors.black38),
            ),
            SizedBox(
              height: 38,
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => RegistrationScreen()),
                  );
                },
                style: ButtonStyle(
                    foregroundColor: MaterialStateProperty.all<Color>(
                        ColorConstants.secondaryColor),
                    backgroundColor: MaterialStateProperty.all<Color>(
                        ColorConstants.primaryColor),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24),
                    ))),
                child: Padding(
                  padding: EdgeInsets.all(14),
                  child: Text(
                    translation(context).home_button_create_account,
                    style: TextStyle(fontSize: 15),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 22,
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {},
                style: ButtonStyle(
                    foregroundColor: MaterialStateProperty.all<Color>(
                        ColorConstants.primaryColor),
                    backgroundColor: MaterialStateProperty.all<Color>(
                        ColorConstants.secondaryColor),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                            side: BorderSide(
                                color: ColorConstants.primaryColor)))),
                child: Padding(
                  padding: EdgeInsets.all(14),
                  child: Text(
                    translation(context).home_button_login,
                    style: TextStyle(fontSize: 15),
                  ),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
