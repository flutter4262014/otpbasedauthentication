import 'package:flutter/material.dart';

import '../allConstants/color_constants.dart';
import '../allConstants/language_constants.dart';

class OtpScreen extends StatefulWidget {
  const OtpScreen({Key? key}) : super(key: key);

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ColorConstants.primaryBackgroungColor,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 24, horizontal: 32),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.black54,
                    size: 20,
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Image.asset(
                'assets/images/Vision_1.png',
                width: 240,
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                translation(context).otp_screen_verification_label,
                style: TextStyle(fontSize: 18),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                translation(context).otp_screen_code_number_textfield,
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.black38),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 18,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 52),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _textFieldOpt(first: true, last: false),
                    _textFieldOpt(first: false, last: false),
                    _textFieldOpt(first: false, last: false),
                    _textFieldOpt(first: true, last: true),
                  ],
                ),
              ),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.all<Color>(
                          ColorConstants.secondaryColor),
                      backgroundColor: MaterialStateProperty.all<Color>(
                          ColorConstants.primaryColor),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                      ))),
                  child: Padding(
                    padding: EdgeInsets.all(14),
                    child: Text(
                      translation(context).otp_screen_verify_button,
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                translation(context).otp_screen_resend_code_message,
                style: TextStyle(
                    fontSize: 14,
                    color: ColorConstants.primaryColor),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _textFieldOpt({required bool first, last}) {
    return Container(
      height: 50,
      child: AspectRatio(
        aspectRatio: 0.8,
        child: TextField(
          autofocus: true,
          onChanged: (value) {
            if (value.length == 1 && last == false) {
              FocusScope.of(context).nextFocus();
            }
            if (value.length == 0 && first == false) {
              FocusScope.of(context).previousFocus();
            }
          },
          showCursor: false,
          readOnly: false,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          keyboardType: TextInputType.number,
          maxLength: 1,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 8),
            counter: Offstage(),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black12),
              borderRadius: BorderRadius.circular(12),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: ColorConstants.primaryColor),
              borderRadius: BorderRadius.circular(12),
            ),
          ),
        ),
      ),
    );
  }
}
