import 'package:flutter/material.dart';

import '../allConstants/color_constants.dart';
import '../allConstants/language_constants.dart';
import 'otp_screen.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ColorConstants.primaryBackgroungColor,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 24, horizontal: 32),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.black54,
                    size: 20,
                  ),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              Image.asset(
                'assets/images/Vision_1.png',
                width: 240,
              ),
              SizedBox(
                height: 75,
              ),
              Text(
                translation(context).registration_message1,
                style: TextStyle(fontSize: 18),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                translation(context).registration_add_phone_number,
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.black38),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 38,
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: ColorConstants.primaryColor),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: ColorConstants.primaryColor),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  prefix: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Text(
                      '(+62)',
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  suffixIcon: Icon(
                    Icons.check_circle,
                    color: Colors.green,
                    size: 32,
                  ),
                ),
              ),
              SizedBox(
                height: 22,
              ),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => OtpScreen()),
                    );
                  },
                  style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.all<Color>(
                          ColorConstants.secondaryColor),
                      backgroundColor: MaterialStateProperty.all<Color>(
                          ColorConstants.primaryColor),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                      ))),
                  child: Padding(
                    padding: EdgeInsets.all(14),
                    child: Text(
                      translation(context).registration_send_phone_number,
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
